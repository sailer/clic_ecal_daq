// This code allows to read a RAM from the FPGA using IPBUS Block.  




// Include libraries 
#include <bitset>
#include <iostream>
#include <string>
#include "uhal/uhal.hpp"
#include "uhal/log/log.hpp"
#include <stdio.h>
#include <fstream>
 
 
 
 
 
int main ( int argc , char** argv )
{
  using namespace uhal;
 
 
 
 
 //Connecting to the device 

    ConnectionManager connectionManager ( "file://~/clic_ecal_daq/IPBUS_Software/Trunk/cfg/connections.xml" );

    HwInterface hw = connectionManager.getDevice ( "pixelTest1" );

    std::cout << "Doing read..." << std::endl;   
 



   // Ask the user for the command to be sent. This command is for the SPI comunication with the ADC. 
   
    int num;
        std::cout << "Enter the # to select the command to send to the ADC"<< std::endl;
        std::cout << "1) 10100000000000000000 -- Normal operation " << std::endl;
        std::cout << "2) 10100000000000000010 -- Adds PLL ON to normal operation" << std::endl;
        std::cout << "3) 10100000000001100000 -- Internal counter with ordinary order of counting" << std::endl;
        std::cout << "4) 10100000000001000000 -- Internal counter with pseudo random order of counting" << std::endl;
    std::cin >> num;
    
    while (num < 1 || num >4)
    {
        std::cout << "Please enter a valid number 1-2-3-4 "<< std::endl;
        std::cout << "1) 10100000000000000000 -- Normal operation " << std::endl;
        std::cout << "2) 10100000000000000010 -- Adds PLL ON to normal operation" << std::endl;
        std::cout << "3) 10100000000001100000 -- Internal counter with ordinary order of counting" << std::endl;
        std::cout << "4) 10100000000001000000 -- Internal counter with pseudo random order of counting" << std::endl;


     // I should add the possibility to enter their own 20 bits number. This is not hard to do, but I am not sure if it is very handy for the user. 
		
        std::cin >> num;
    }
    
     


   
   
   // Create the word that will be sent:  Once the number is entered, we choose one of the values from the table. Instead of having several registers, it was easier to change the bits that were different betwen the words.  
   
    std::bitset<32> SPI_Command ;
    
    SPI_Command = 2621440;
    
   
       if (num ==1) {
  
     	}
		else  if (num ==2){
	       SPI_Command[3] = 1;    // Changes the 3th bit from '0' to '1'
	    }
		else  if (num ==3){
	       SPI_Command[7] = 1;    // Changes the 7th bit from '0' to '1'
	       SPI_Command[8] = 1;	  // Changes the 8th bit from '0' to '1'     
	    }
	    else  if (num ==4){
	       SPI_Command[8] = 1;   // Changes the 8th bit from '0' to '1'
	    }
       
//       std::cout << SPI_Command << " as an integer is: " << SPI_Command.to_ulong() << '\n';

   
     
     
     
     
     
    //read the register existent in the FPGA    : We read the register than was stored in the FPGA. Then we toggle the bit-1, so we can trigger an action. 
 
    ValWord< uint32_t > reg = hw.getNode ("ctrl_reg").read();
   
    hw.dispatch();
    
    // // Print the value (normally it is zero at the start)
    // std::cout << reg.value() << std::endl;
   
    
    // Create a BITSET of the register read :  This makes easier the manipulation of the bits
    
    std::bitset<32> reg_FPGA ;
    reg_FPGA=reg.value();    // We assign the value of the register from the FPGA (reg) to the bitset register (reg_FPGA)
 
 
 
   //Modify the register of the user, by flipping the bit number 1
    
    SPI_Command[1]= not reg_FPGA[1];     // Then we toggle the bit-1, so we can trigger an action. We save the value in SPI_Command that will be written in the FPGA.  


   //write the register back to the FPGA in the address 0x0001
   hw.getNode ("ctrl_reg").write(SPI_Command.to_ulong());

   //send the IPbus transactions
   hw.dispatch();



   //std::cout << "ctrl_reg = " << reg.value() << std::endl;
 
 
 
 // Just to make the script wait. I will make this better soon
 
 
    int numero;
        std::cout << "read data?   write 0 when ready"  << std::endl;
       
    std::cin >> numero;
    
    while (numero > 0 )
       std::cin >> numero;

   







  //Memory RAM part



   
   
  //read back the information from the ram

	ValVector< uint32_t > mem = hw.getNode ( "ram" ).readBlock (1024);  // reads 1024 words of 32 bits

	hw.dispatch();



    std::bitset<32> ram_FPGA ;       


    std::bitset<10> add_FPGA ;
    std::bitset<10> ch1_FPGA ;
    std::bitset<10> ch2_FPGA ;


//    ram_FPGA=mem.value();

// Once we get the data from the memory, we save it in bitset. So we can print it easily.   Bits from 31 to 22 correspond to the 10-bits address. Bits from 19 to 10 correspond to one channel 10-bits data.  Bits from 9 to 0 correspond to another channel 10-bits data.

	for(size_t i=0; i!= 1024; ++i)    {
        ram_FPGA=mem[i];
        
        for(size_t f=0; f!= 10; ++f)
        add_FPGA[9-f]=ram_FPGA[31-f];

        for(size_t g=0; g!= 10; ++g)
        ch1_FPGA[g]=ram_FPGA[g];
        
        for(size_t h=0; h!= 10; ++h)
        ch2_FPGA[9-h]=ram_FPGA[19-h];


	printf ("-- %u - %u - %u --", add_FPGA ,ch1_FPGA ,ch2_FPGA  );   // There are 8 channels. 2 are read out at the same time. We send 1024/4 samples of each pair of channel. 0-1, 2-3, 4-5, 6-7  
  	  
     	}



 
   
 
  return 0;
}





 

 

