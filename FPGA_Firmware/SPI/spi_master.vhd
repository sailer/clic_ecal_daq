-- SPI Master
-- 
-- For the time being, we are just communicating with one ADC ASIC. To communicate with more (4 for example), we just have to change the tx_data. The code remains the same. 

LIBRARY ieee;

USE ieee.std_logic_1164.all;

USE ieee.std_logic_arith.all;

USE ieee.std_logic_unsigned.all;



ENTITY spi_master IS

  GENERIC(

    d_width : INTEGER := 20); --data bus width for ADC (command). 

  PORT(

	--INPUTS
	
    clock   : IN     STD_LOGIC;                             --clock in

    clk_div : IN     INTEGER;                               --system clock cycles per 1/2 period of sclk. freq_sclk = freq_clock/(2 * clk_div)  


    reset_n : IN     STD_LOGIC;                             --asynchronous reset ( '1' to make the block work. '0' if we want it off)

    enable  : IN     STD_LOGIC;                             --initiate transaction when '1'. Just high for 1 clock or two, '0' the rest. 

	 	 
    tx_data : IN     STD_LOGIC_VECTOR(d_width-1 DOWNTO 0);  --data to transmit. The data as well tells wich of the four ADC we are communicating with.
	 
 



	--OUTPUTS
	
    sclk    : BUFFER STD_LOGIC;                              --spi clock 

    mosi    : OUT    STD_LOGIC;                              --master out, slave in

    busy    : OUT    STD_LOGIC);                             --busy / data ready signal. '1'=> working. '0'=> ready to work. 
	 
END spi_master;



ARCHITECTURE logic OF spi_master IS

  TYPE machine IS(ready, execute);                           --state machine data type

  SIGNAL state       : machine;                              --current state

  SIGNAL clk_ratio   : INTEGER;                              --current clk_div

  SIGNAL count       : INTEGER:=0;                              --counter to trigger sclk from system clock

  SIGNAL clk_toggles : INTEGER RANGE 0 TO d_width*2 + 1;     --count spi clock toggles

  SIGNAL tx_buffer   : STD_LOGIC_VECTOR(d_width-1 DOWNTO 0); --transmit data buffer

  SIGNAL last_bit_rx : INTEGER RANGE 0 TO d_width*2;         --last rx data bit location  
  
  SIGNAL assert_data : STD_LOGIC;                            --'1' is tx sclk toggle, '0' is rx sclk toggle

  SIGNAL sending_data_n : STD_LOGIC;                         -- Tells when the data is being sent. Needed for avoiding a toogle in the scl at the first time the function is called. '0'=> data being sent  

BEGIN

  PROCESS(clock, reset_n)

  BEGIN



    IF(reset_n = '0') THEN        --reset system 

      busy <= '1';                --set busy signal
				
      state <= ready;             --go to ready state when reset is exited

      mosi <= 'Z';                --set master out to high impedance
		
		sending_data_n <=  '1';               -- data not being sent



    ELSIF(clock'EVENT AND clock = '1') THEN

      CASE state IS               --state machine


        WHEN ready =>

          busy <= '0';             --clock out not busy signal

          mosi <= 'Z';             --set mosi output high impedance
			                         
			 sending_data_n <= '1';     --It means that the data is not being sent yet

 
          --user input to initiate transaction

          IF(enable = '1') THEN       

            busy <= '1';             --set busy signal
				

            IF(clk_div = 0) THEN     --check for valid spi speed

              clk_ratio <= 1;        --set to maximum speed if zero

              count <= 1;            --initiate system-to-spi clock counter

            ELSE

              clk_ratio <= clk_div;  --set to input selection if valid

              count <= clk_div;      --initiate system-to-spi clock counter

            END IF;

 

				sclk <= '0';            --set spi clock polarity
				
				assert_data <= '1'; --set spi clock phase


            tx_buffer <= tx_data;    --clock in data for transmit into buffer

            clk_toggles <= 0;        --initiate clock toggle counter

            last_bit_rx <= d_width*2 - 1; --set last rx data bit

            state <= execute;        --proceed to execute state

          ELSE

            state <= ready;          --remain in ready state

          END IF;



        WHEN execute =>

          busy <= '1';        --set busy signal
          
          sending_data_n <= '0';     -- Data is being sent


          --system clock to sclk ratio is met

          IF(count = clk_ratio) THEN        

            count <= 1;                     --reset system-to-spi clock counter
			   assert_data <= NOT assert_data; --switch transmit indicator

				
				IF(clk_toggles = d_width*2 + 1) THEN

              clk_toggles <= 0;               --reset spi clock toggles counter

            ELSE

              clk_toggles <= clk_toggles + 1; --increment spi clock toggles counter

            END IF;

            

            --spi clock toggle needed

            IF(clk_toggles <= d_width*2 AND sending_data_n='0') THEN  --just toogle the clock when data is being sent
				
              sclk <= NOT sclk; --toggle spi clock  

            END IF;

        
           --transmit spi clock toggle 

           
           IF(assert_data = '1' AND clk_toggles < last_bit_rx) THEN  --transmit the remaining MSB to the mosi output

              mosi <= tx_buffer(d_width-1);                     --clock out data bit

              tx_buffer <= tx_buffer(d_width-2 DOWNTO 0) & '0'; --shift data transmit buffer
		     END IF;

          

                       

            --end of transaction

            IF((clk_toggles = d_width*2 + 1)) THEN   

              busy <= '0';             --clock out not busy signal
 
              mosi <= 'Z';             --set mosi output high impedance

              state <= ready;          --return to ready state
				  
				  sending_data_n <= '1';             -- We ended the transaction. Data is not being sent at this stage. 


            ELSE                       --not end of transaction

              state <= execute;        --remain in execute state

            END IF;

            



          ELSE        --system clock to sclk ratio not met

            count <= count + 1; --increment counter

            state <= execute;   --remain in execute state

          END IF;



      END CASE;

    END IF;

  END PROCESS; 

END logic;


