-- Top-level design  
--
-- This version is for xc6slx45 on Digilent ATLYS board

-- This design Includes:
-- 1)  the SPI block for sending data from the FPGA to the ADCs.
-- 2)  Clock Reference Block for sending the high frequency clock (Ref clock, which is 10 times faster than the ADC clock) to the ADC. 
-- 3)  SERDES block to read the serial data from the ADC. 
-- 4)  IPBUS to transfer data from the PC to the FPGA. 


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
Library UNISIM;
use UNISIM.vcomponents.all;
use work.ipbus.all;

entity top_level is port(
  
	--System clock 100MHz
	sysclk: in STD_LOGIC;
	
	-- SERDES  		  --INPUTS
	
   RX0_LVDS : in  STD_LOGIC_VECTOR (9 downto 0);  --Positive channels : I will write here wich number is each channel (drawing will be good)
   RX0_LVDSB : in  STD_LOGIC_VECTOR (9 downto 0);  --Negative channels : (0)-> CLOCK  (1)-> start  (2)-> Data0 (channel 0 ASIC)
		
   -- SERDES        --OUTPUTS
	out_word_Fast_Debug : out  STD_LOGIC_VECTOR (9 downto 0);

	-- SPI	        --INPUTS
	--enable_SPI :in std_logic;   --Enable SPI (we need a square) --In future it will come from IPBUS
 	
	-- SPI           --OUTPUTS
   command_SPI :out std_logic; --output Send the data to the slow control of ADC  
   sclk_SPI :out std_logic;--output Slow clock to slow control of ADC   
   
   SLOW_RST_n_ADC: out std_logic; --output to reset the ADC. Active low

	-- Ref_Clock      --OUTPUTS
   INSEL_CLK_ADC : out std_logic; --output to select the source of clock for the ADC.
	 			  
   --New lines --  
  	JB : out std_logic; --clock slow adc generated from 200MHz
	-------------
				
	CLK_OUT_p : out std_logic;    --differential fast clock for 
	CLK_OUT_n : out std_logic;

	
	--IPBBUS
	leds: out STD_LOGIC_VECTOR(3 downto 0);
	gmii_gtx_clk, gmii_tx_en, gmii_tx_er : out STD_LOGIC;
	gmii_txd : out STD_LOGIC_VECTOR(7 downto 0);
	gmii_rx_clk, gmii_rx_dv, gmii_rx_er: in STD_LOGIC;
	gmii_rxd : in STD_LOGIC_VECTOR(7 downto 0);
	phy_rstb : out STD_LOGIC;
			
   --LED_REG : OUT std_logic;

	dip_switch: in std_logic_vector(3 downto 0)
 
	);
end top_level;

architecture rtl of top_level is
   TYPE machine IS(ready, execute);                           --state machine data type

   SIGNAL state       : machine;                              --current state
	

	COMPONENT ADC_Readout
	PORT(
		-- Inputs
		LVDSclk_p : IN std_logic;
		LVDSclk_n : IN std_logic;
		LVDS_Start_p : IN std_logic;
		LVDS_Start_n : IN std_logic;
		LVDS0_p : IN std_logic;
		LVDS0_n : IN std_logic;
		LVDS1_p : IN std_logic;
		LVDS1_n : IN std_logic;
		LVDS2_p : IN std_logic;
		LVDS2_n : IN std_logic;
		LVDS3_p : IN std_logic;
		LVDS3_n : IN std_logic;
		LVDS4_p : IN std_logic;
		LVDS4_n : IN std_logic;
		LVDS5_p : IN std_logic;
		LVDS5_n : IN std_logic;
		LVDS6_p : IN std_logic;
		LVDS6_n : IN std_logic;
		LVDS7_p : IN std_logic;
		LVDS7_n : IN std_logic;
		
		
		
		
		exrst : IN std_logic;          
		
		-- Outputs
		reset : OUT std_logic;
		
		pclk : OUT std_logic;
		pclkx2 : OUT std_logic;
		pclkx10 : OUT std_logic;
		
		pllclk0 : OUT std_logic;
		pllclk1 : OUT std_logic;
		pllclk2 : OUT std_logic;
		
		pll_lckd : OUT std_logic;
		sdout : OUT std_logic_vector(79 downto 0);
		serdesstrobe : OUT std_logic
		);
	END COMPONENT;
	
	-- Outputs
	SIGNAL rx0_reset :  std_logic;
	
	SIGNAL rx0_pclk :  std_logic;
   SIGNAL rx0_pclkx2 :  std_logic;
   SIGNAL rx0_pclkx10 :  std_logic;
  
   SIGNAL rx0_pllclk0 :  std_logic;
   SIGNAL rx0_pllclk1 :  std_logic;
   SIGNAL rx0_pllclk2 :  std_logic;
	
	SIGNAL rx0_plllckd :  std_logic;
 	SIGNAL RX0_sdout :  std_logic_vector(79 DOWNTO 0);	
   SIGNAL rx0_serdesstrobe :  std_logic;

 

  COMPONENT spi_master

	PORT(
		-- Inputs
		clock : IN std_logic;

		clk_div : IN INTEGER;

		reset_n : IN std_logic;

		enable : IN std_logic;

		tx_data : IN std_logic_vector(19 downto 0);          

		-- Outputs
		sclk : BUFFER std_logic;

		mosi : OUT std_logic;

		busy : OUT std_logic
		
		);

	END COMPONENT;

 
 	 --Inputs


   signal reset_n_SPI : std_logic := '1';
   signal clk_div_SPI : INTEGER := 2;
   

   signal tx_data_SPI : std_logic_vector(19 downto 0) := "10100000000000000000"; --10100000000000000000 the value we will use



 	--Outputs

   signal command_SPI_o : std_logic;

   signal busy_SPI : std_logic:='0';

	signal sclk_SPI_o : std_logic;



 	COMPONENT Ref_Clock_ADC

	PORT(
	   -- Inputs

		CLK_IN1 : IN std_logic;

		RESET : IN std_logic;          


	   -- Outputs

		CLK_OUT_slow: OUT std_logic;

		CLK_OUT_p : OUT std_logic;

		CLK_OUT_n : OUT std_logic;

		LOCKED : OUT std_logic

		);

	END COMPONENT;

    signal LOCKED_CLK_ADC_REF : std_logic;

    signal CLK_OUT_slow_int : std_logic;

	 

	 signal CLK_OUT_p_int: std_logic;

	 signal CLK_OUT_n_int: std_logic;


	COMPONENT top
	PORT(
	  -- datain  : in STD_LOGIC_VECTOR(31 downto 0);
	   datain: in STD_LOGIC_VECTOR(79 downto 0);
      clockin: IN std_logic;
      Flag_readout: IN std_logic;


		sysclk : IN std_logic;
		gmii_rx_clk : IN std_logic;
		gmii_rx_dv : IN std_logic;
		gmii_rx_er : IN std_logic;
		gmii_rxd : IN std_logic_vector(7 downto 0);
		dip_switch : IN std_logic_vector(3 downto 0);          
      ctrl_reg_SPI: out STD_LOGIC_VECTOR(31 downto 0);
		leds : OUT std_logic_vector(3 downto 0);
		gmii_gtx_clk : OUT std_logic;
		gmii_tx_en : OUT std_logic;
		gmii_tx_er : OUT std_logic;
		gmii_txd : OUT std_logic_vector(7 downto 0);
		phy_rstb : OUT std_logic
		);
	END COMPONENT;

	  SIGNAL enable_SPI_int :  std_logic :='0';
	  SIGNAL enable_SPI_int_b:  std_logic :='0';
	  SIGNAL enable_SPI :  std_logic :='0';
	  SIGNAL count_enable_SPI : integer := 0;
	  SIGNAL ctrl_reg_SPI_int : STD_LOGIC_VECTOR(31 downto 0):= X"00000000";
	  
  	  SIGNAL Flag_readout :  std_logic :='0'; --to delete afterwards 

	
begin


	--Assign values 
	INSEL_CLK_ADC  <= '1';   --output to select the source of clock for the ADC. '1' Selects the VHDCI source
	SLOW_RST_n_ADC <= '1';   --output to reset the ADC. Active low. 
  	reset_n_SPI <= '1';
	clk_div_SPI <= 2;        -- clock dicision for the slow clock respect to the input
	
	
    spi_ADC: spi_master PORT MAP(
	 
		-- Inputs
		clock => CLK_OUT_slow_int, --CLK_IN1 (Input clock normally the slow one comming from the Ref_Clock block)

 		clk_div => clk_div_SPI,

		reset_n => '1',--reset_n_SPI

		enable =>enable_SPI , --.enable_SPI

 		tx_data =>tx_data_SPI ,

		-- Outputs
		sclk => sclk_SPI_o,

		mosi => command_SPI_o,

	   busy => busy_SPI
	);

		sclk_SPI<=sclk_SPI_o;

		command_SPI<=command_SPI_o; 
		
		
		
		
	Inst_Ref_Clock_ADC: Ref_Clock_ADC PORT MAP(
		-- Inputs
		CLK_IN1 => sysclk,
		
		RESET => '0',

		-- Outputs
		CLK_OUT_slow =>CLK_OUT_slow_int ,

		CLK_OUT_p =>CLK_OUT_p_int ,

		CLK_OUT_n => CLK_OUT_n_int,

		LOCKED => LOCKED_CLK_ADC_REF

	);

 

		CLK_OUT_p<= CLK_OUT_p_int;
		CLK_OUT_n<= CLK_OUT_n_int; 	
           
		tx_data_SPI <= ctrl_reg_SPI_int(21 downto 2);
		
 

		
		
	ADC_rx0: ADC_Readout PORT MAP(     -- The PCB Has an Inverter in the LVDS lines
	 --These are input ports
		LVDSclk_p => RX0_LVDS(0),                
		LVDSclk_n => RX0_LVDSB(0) ,
		LVDS_Start_p => RX0_LVDS(1),
		LVDS_Start_n => RX0_LVDSB(1),
		LVDS0_p => RX0_LVDS(2),
		LVDS0_n => RX0_LVDSB(2),
		
		LVDS1_p => RX0_LVDS(3),
		LVDS1_n => RX0_LVDSB(3),
		LVDS2_p => RX0_LVDS(4),
		LVDS2_n => RX0_LVDSB(4),
		LVDS3_p => RX0_LVDS(5),
		LVDS3_n => RX0_LVDSB(5),
		LVDS4_p => RX0_LVDS(6),
		LVDS4_n => RX0_LVDSB(6),
		LVDS5_p => RX0_LVDS(7),
		LVDS5_n => RX0_LVDSB(7),
		LVDS6_p => RX0_LVDS(8),
		LVDS6_n => RX0_LVDSB(8),
		LVDS7_p => RX0_LVDS(9),
		LVDS7_n => RX0_LVDSB(9),
		
		exrst => '0', -- Add a variable	 --  Neg(rstbtn_n)	 
	 --These are output ports|
		 
		reset => rx0_reset ,
		
		pclk => rx0_pclk ,
		pclkx2 => rx0_pclkx2,
		pclkx10 => rx0_pclkx10,
		
		pllclk0 => rx0_pllclk0,
		pllclk1 => rx0_pllclk1,
		pllclk2 => rx0_pllclk2,
		
		pll_lckd => rx0_plllckd,
		sdout => RX0_sdout,
		serdesstrobe => rx0_serdesstrobe
	);

     out_word_Fast_Debug<=   RX0_sdout(79 downto 70);   
     JB <= rx0_pclk; 
 


     --RX0_sdout; rx0_pclk;              --add in Inst-Top as inputs
 
      Inst_top: top PORT MAP(
      datain=>RX0_sdout,
      clockin=>RX0_pclk,
 		Flag_readout=>Flag_readout,
		
		sysclk =>CLK_OUT_slow_int ,
		ctrl_reg_SPI => ctrl_reg_SPI_int,
		leds =>leds ,
		gmii_gtx_clk => gmii_gtx_clk,
		gmii_tx_en =>gmii_tx_en ,
		gmii_tx_er =>gmii_tx_er ,
		gmii_txd =>gmii_txd ,
		gmii_rx_clk =>gmii_rx_clk ,
		gmii_rx_dv =>gmii_rx_dv ,
		gmii_rx_er =>gmii_rx_er ,
		gmii_rxd => gmii_rxd,
		phy_rstb => phy_rstb,
		dip_switch => dip_switch
	);


---------------------------------------

 

   
 
enable_SPI_int<= ctrl_reg_SPI_int(1);
 
PROCESS(CLK_OUT_slow_int, enable_SPI_int)

  BEGIN
    IF(CLK_OUT_slow_int'EVENT AND CLK_OUT_slow_int = '1') THEN

      CASE state IS               --state machine


        WHEN ready =>

          IF enable_SPI_int_b = enable_SPI_int THEN
            count_enable_SPI <= 0;            -- 

				enable_SPI  <='0';
            state <= ready;           

				
          ElSE  
			 			 state <= execute;          
                   enable_SPI<='1'; 
          

     END IF;



        WHEN execute =>

 
 
  
          IF(count_enable_SPI < 5) THEN        

            count_enable_SPI <= count_enable_SPI + 1;                     
				enable_SPI<='1'; 
				--flag to test readout
				Flag_readout<='1';
				
           ELSE
            enable_SPI_int_b<=enable_SPI_int;
            state <= ready;
 				enable_SPI<='0'; 

            END IF;

            
 

      END CASE;

    END IF;

  END PROCESS; 







end rtl;
