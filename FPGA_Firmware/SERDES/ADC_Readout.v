// SERDES BLOCK to read out the ADC ASICs

 
`timescale 1 ns / 1ps

module ADC_Readout (
  
  //INPUTS
  input  wire LVDSclk_p,      // LVds clock in  (fast clock around 200MHz) 
  input  wire LVDSclk_n,      // LVds clock in
  
  input  wire LVDS_Start_p,   // LVDS0 data in Start to select a word
  input  wire LVDS_Start_n,   // LVDS0 data in Start to select a word
  
  input  wire LVDS0_p,         // LVDS0 data in
  input  wire LVDS0_n,         // LVDS0 data in
  
  input  wire LVDS1_p,         // LVDS1 data in
  input  wire LVDS1_n,         // LVDS1 data in

  input  wire LVDS2_p,         // LVDS2 data in
  input  wire LVDS2_n,         // LVDS2 data in

  input  wire LVDS3_p,         // LVDS3 data in
  input  wire LVDS3_n,         // LVDS3 data in

  input  wire LVDS4_p,         // LVDS4 data in
  input  wire LVDS4_n,         // LVDS4 data in

  input  wire LVDS5_p,         // LVDS5 data in
  input  wire LVDS5_n,         // LVDS5 data in
  
  input  wire LVDS6_p,         // LVDS6 data in
  input  wire LVDS6_n,         // LVDS6 data in

  input  wire LVDS7_p,         // LVDS7 data in
  input  wire LVDS7_n,         // LVDS7 data in

  
  

  input  wire exrst,          // external reset input, e.g. reset button

  
  //OUTPUTS
  output wire reset,          // rx reset
  
  output wire pclk,           // regenerated pixel clock
  output wire pclkx2,         // double rate pixel clock
  output wire pclkx10,        // 10x pixel clock. This is the frequency of the clock (ref clock) sent by the ADC ASIC 
  
  output wire pllclk0,        // send pllclk0 out so it can be fed into a different BUFPLL
  output wire pllclk1,        // PLL x1 output
  output wire pllclk2,        // PLL x2 output

  output wire pll_lckd,       // send pll_lckd out so it can be fed into a different BUFPLL
  
  output reg [79:0] sdout,    // output word. bit 79 to 69 for channel 0, MSB first. bit 09 to 00 channel 7, MSB first (in bit 09). 
  
  output wire serdesstrobe);   // BUFPLL serdesstrobe output
  
 
   
 
   
  ////////////////////////////////////////////////////////////////////////////////////////////////
  //
  // START: CREATING THE CLOCKS
  //
  ////////////////////////////////////////////////////////////////////////////////////////////////
  
 
  // Send LVDS clock to a differential buffer and then a BUFIO2
  // This is a required path in Spartan-6 feed a PLL CLKIN
  
  //                                       .Divide(5)
  //               ________                _________
  // LVDSclk_p  --|        \               |        \ 
  //              | IBUFDS  >-- rxclkint --| BUFIO2  >-- rxclk  
  // LVDSclk_n  -o|________/               |________/             

  //              ibuf_rxclk              bufio_LVDSclk            

  wire rxclkint;
  
  //SelectIO resources Spartan-6 differential IBUFDS (Differential input buffer primitive)
  IBUFDS  #(.IOSTANDARD("LVDS_25"), .DIFF_TERM("FALSE"))      
  ibuf_rxclk (.I(LVDSclk_p), .IB(LVDSclk_n), .O(rxclkint));   
  
  wire rxclk;

  BUFIO2 #(.DIVIDE_BYPASS("FALSE"), .DIVIDE(5))
  bufio_LVDSclk (.DIVCLK(rxclk), .IOCLK(), .SERDESSTROBE(), .I(rxclkint));

   

 
  // PLL is used to generate three clocks:
  // 1. pclk:    
  // 2. pclkx2:  double rate of pclk used for 5:10 soft gear box and ISERDES DIVCLK
  // 3. pclkx10: 10x rate of pclk used as IO clock (same rate as incoming LVDS CLOCK from ADC)
 
 
  
  //             .CLKOUT2_DIVIDE(5)
  //             .CLKOUT1_DIVIDE(10)
  //             .CLKOUT0_DIVIDE(1)
  //              .CLKFBOUT_MULT(5)
  //              .CLKIN_PERIOD(10)                   
  //              |          |-- pllclk1 -- |BUFG>--    pclk                 |        |
  //              |          |             pclkbufg                          |        | 
  //     rxclk  --| PLL_BASE |-- pllclk2 -- |BUFG>--    pclkx2   -- (.GCLK)  | BUFPLL | -- pclkx10
  // 	            |          |             pclkx2bufg                        |		  |    
  //		         |__________|-- pllclk0 --             pllclk0  -- (.PLLIN) |________|

  //               PLL_ISERDES                                                ioclk_buf
  
  PLL_BASE # (
    .CLKIN_PERIOD(10),   //100MHz
    .CLKFBOUT_MULT(5),  //set VCO to 1x of CLKIN (the fast clock) // 1 to 64 //Specifies the amount to multiply all CLKOUT clock outputs if a different frequency is desired. This number, in combination with the associated CLKOUT#_DIVIDE value and DIVCLK_DIVIDE value, will determine the output frequency.
    .CLKOUT0_DIVIDE(1), //x10 clk   
    .CLKOUT1_DIVIDE(10),//x1 clk    
    .CLKOUT2_DIVIDE(5), //x2 clk  
    .COMPENSATION("INTERNAL")
  ) PLL_ISERDES (
        
    //INPUTS

	 .CLKIN(rxclk),        //General Clock Input
	 .CLKFBIN(clkfbout),   //Feedback clock input.
    .RST(exrst),          //Asynchronous reset signal. The RST signal is an asynchronous reset for the PLL.

    //OUTPUTS

	 .CLKFBOUT(clkfbout),  //Dedicated PLL feedback output
    .CLKOUT0(pllclk0),    //x10 //User configurable clock outputs (0 through 5)
    .CLKOUT1(pllclk1),    //x1  //that can be divided versions of the VCO phase outputs (user controllable) from 1 (bypassed) to 128.
    .CLKOUT2(pllclk2),    //x2  //The input clock and output clocks are phase aligned.
    .CLKOUT3(),// NOT USED
    .CLKOUT4(),// NOT USED
    .CLKOUT5(),// NOT USED
    .LOCKED(pll_lckd)    //Asynchronous output from the PLL that indicates when the PLL has achieved phase alignment within a predefined window and frequency matching within a predefined PPM range. 
        );

  //////////////////////////////////////////////////////////////////
  // pclk clock buffer
  //////////////////////////////////////////////////////////////////

  BUFG pclkbufg (.I(pllclk1), .O(pclk));

  //////////////////////////////////////////////////////////////////
  // 2x pclk is going to be used to drive IOSERDES2 DIVCLK
  //////////////////////////////////////////////////////////////////

  BUFG pclkx2bufg (.I(pllclk2), .O(pclkx2));

  //////////////////////////////////////////////////////////////////
  // 10x pclk is used to drive IOCLK network so a bit rate reference
  // can be used by IOSERDES2 
  //
  //  Intended for HIGH SPEED I/O routing to generate clocks and strobe
  //  pulses for the ISERDES2 (SDR) primitives
  //////////////////////////////////////////////////////////////////
  
  wire bufpll_lock;  
  BUFPLL #(.DIVIDE(5))  //.DIVIDE(5) //Sets the PLLIN divider divide-by value for SERDESSTROBE
  ioclk_buf (
  //Inputs
  .PLLIN(pllclk0),  //Clock input from PLL (CLKOUT0, CLKOUT1) directly connected to the PLL
  .GCLK(pclkx2),    //Clock input from BUFG or GCLK.¡¡¡¡¡¡¡The GCLK frequency must match the expected SERDESSTROBE frequency!!!!!!!. FGCLK = FPLLIN/DIVIDE
  .LOCKED(pll_lckd),// LOCKED signal from PLL.
  //Outputs
  .IOCLK(pclkx10),  //I/O clock network output. Connects to IOSERDES2 (CLK0)
  .SERDESSTROBE(serdesstrobe), //I/O clock network output used to drive IOSERDES2 (IOCE).
  .LOCK(bufpll_lock)); //Synchronized LOCK output directly connected to the PLL.


   
  ////////////////////////////////////////////////////////////////////////////////////////////////
  //
  // END: CREATING THE CLOCKS
  //
  ////////////////////////////////////////////////////////////////////////////////////////////////
  
  
  
  
  
  
  
  
  ////////////////////////////////////////////////////////////////////////////////////////////////
  //
  // START: Data Reading Channels   
  //
  ////////////////////////////////////////////////////////////////////////////////////////////////


  // Here we should call others functions to read all the channels

  
  assign reset = ~bufpll_lock;
  
  
  
  // Reads Channel START    
  wire [4:0] sdoutStart;  

  decode LVDS_Start (          // Reads Channel Start of the ADC
    //INPUTS
    .reset        (reset),
    .pclk         (pclk),
    .pclkx2       (pclkx2),
    .pclkx10      (pclkx10),
    .serdesstrobe (serdesstrobe),
    .din_p        (LVDS_Start_p),
    .din_n        (LVDS_Start_n),

	 //OUTPUTS

    .sdout        (sdoutStart)) ;


  // Reads Channel LVDS0    
  wire [4:0] sdout0;     
  
  decode LVDS_0 (          // Reads Channel 0 of the ADC
    //INPUTS
    .reset        (reset),
    .pclk         (pclk),
    .pclkx2       (pclkx2),
    .pclkx10      (pclkx10),
    .serdesstrobe (serdesstrobe),
    .din_p        (LVDS0_p),
    .din_n        (LVDS0_n),

	 //OUTPUTS

    .sdout        (sdout0)) ;



// Reads Channel LVDS1    
  wire [4:0] sdout1;     
  
  decode LVDS_1 (          // Reads Channel 1 of the ADC
    //INPUTS
    .reset        (reset),
    .pclk         (pclk),
    .pclkx2       (pclkx2),
    .pclkx10      (pclkx10),
    .serdesstrobe (serdesstrobe),
    .din_p        (LVDS1_p),
    .din_n        (LVDS1_n),

	 //OUTPUTS

    .sdout        (sdout1)) ;
//	 
	 
	 
  // Reads Channel LVDS2    
  wire [4:0] sdout2;     
  
  decode LVDS_2 (          // Reads Channel 2 of the ADC
    //INPUTS
    .reset        (reset),
    .pclk         (pclk),
    .pclkx2       (pclkx2),
    .pclkx10      (pclkx10),
    .serdesstrobe (serdesstrobe),
    .din_p        (LVDS2_p),
    .din_n        (LVDS2_n),

	 //OUTPUTS

    .sdout        (sdout2)) ;


  // Reads Channel LVDS3    
  wire [4:0] sdout3;     
  
  decode LVDS_3 (          // Reads Channel 3 of the ADC
    //INPUTS
    .reset        (reset),
    .pclk         (pclk),
    .pclkx2       (pclkx2),
    .pclkx10      (pclkx10),
    .serdesstrobe (serdesstrobe),
    .din_p        (LVDS3_p),
    .din_n        (LVDS3_n),

	 //OUTPUTS

    .sdout        (sdout3)) ;
	 
	 
	   // Reads Channel LVDS4    
  wire [4:0] sdout4;     
  
  decode LVDS_4 (          // Reads Channel 4 of the ADC
    //INPUTS
    .reset        (reset),
    .pclk         (pclk),
    .pclkx2       (pclkx2),
    .pclkx10      (pclkx10),
    .serdesstrobe (serdesstrobe),
    .din_p        (LVDS4_p),
    .din_n        (LVDS4_n),

	 //OUTPUTS

    .sdout        (sdout4)) ;
	 
	 
	 
	   // Reads Channel LVDS5    
  wire [4:0] sdout5;     
  
  decode LVDS_5 (          // Reads Channel 5 of the ADC
    //INPUTS
    .reset        (reset),
    .pclk         (pclk),
    .pclkx2       (pclkx2),
    .pclkx10      (pclkx10),
    .serdesstrobe (serdesstrobe),
    .din_p        (LVDS5_p),
    .din_n        (LVDS5_n),

	 //OUTPUTS

    .sdout        (sdout5)) ;
//	 
//	 
//	 
	    // Reads Channel LVDS6    
  wire [4:0] sdout6;     
  
  decode LVDS_6 (          // Reads Channel 6 of the ADC
    //INPUTS
    .reset        (reset),
    .pclk         (pclk),
    .pclkx2       (pclkx2),
    .pclkx10      (pclkx10),
    .serdesstrobe (serdesstrobe),
    .din_p        (LVDS6_p),
    .din_n        (LVDS6_n),

	 //OUTPUTS

    .sdout        (sdout6)) ;
	 
	   // Reads Channel LVDS7    
  wire [4:0] sdout7;     
  
  decode LVDS_7 (          // Reads Channel 7 of the ADC
    //INPUTS
    .reset        (reset),
    .pclk         (pclk),
    .pclkx2       (pclkx2),
    .pclkx10      (pclkx10),
    .serdesstrobe (serdesstrobe),
    .din_p        (LVDS7_p),
    .din_n        (LVDS7_n),

	 //OUTPUTS

    .sdout        (sdout7)) ;

//
//


  
  
  
  ////////////////////////////////////////////////////////////////////////////////////////////////
  //
  // End: Data Reading Channels   
  //
  ////////////////////////////////////////////////////////////////////////////////////////////////











  ////////////////////////////////////////////////////////////////////////////////////////////////
  //
  // Start:    Word Creation  
  //
  ////////////////////////////////////////////////////////////////////////////////////////////////

   reg [4:0] raw5bit_Start;  // Start data now


   // Each LVDS data line will need these registers
   reg [4:0] raw5bit_LVDS0;  // LVDS0 data now
	reg [4:0] raw5bit_q_LVDS0;// LVDS0 data previous
   reg [4:0] raw5bit_qq_LVDS0;// LVDS0 data previoius previous
	reg [9:0] Finalword_LVDS0;// Final LVDS0 data word (Not in sync with "pclk")

   // Each LVDS data line will need these registers
   reg [4:0] raw5bit_LVDS1;  // LVDS0 data now
	reg [4:0] raw5bit_q_LVDS1;// LVDS0 data previous
   reg [4:0] raw5bit_qq_LVDS1;// LVDS0 data previoius previous
	reg [9:0] Finalword_LVDS1;// Final LVDS0 data word (Not in sync with "pclk")

	// Each LVDS data line will need these registers
   reg [4:0] raw5bit_LVDS2;  // LVDS0 data now
	reg [4:0] raw5bit_q_LVDS2;// LVDS0 data previous
   reg [4:0] raw5bit_qq_LVDS2;// LVDS0 data previoius previous
	reg [9:0] Finalword_LVDS2;// Final LVDS0 data word (Not in sync with "pclk")

	// Each LVDS data line will need these registers
   reg [4:0] raw5bit_LVDS3;  // LVDS0 data now
	reg [4:0] raw5bit_q_LVDS3;// LVDS0 data previous
   reg [4:0] raw5bit_qq_LVDS3;// LVDS0 data previoius previous
	reg [9:0] Finalword_LVDS3;// Final LVDS0 data word (Not in sync with "pclk")


	// Each LVDS data line will need these registers
   reg [4:0] raw5bit_LVDS4;  // LVDS0 data now
	reg [4:0] raw5bit_q_LVDS4;// LVDS0 data previous
   reg [4:0] raw5bit_qq_LVDS4;// LVDS0 data previoius previous
	reg [9:0] Finalword_LVDS4;// Final LVDS0 data word (Not in sync with "pclk")

	 //Each LVDS data line will need these registers
   reg [4:0] raw5bit_LVDS5;  // LVDS0 data now
	reg [4:0] raw5bit_q_LVDS5;// LVDS0 data previous
   reg [4:0] raw5bit_qq_LVDS5;// LVDS0 data previoius previous
	reg [9:0] Finalword_LVDS5;// Final LVDS0 data word (Not in sync with "pclk")

	// Each LVDS data line will need these registers
   reg [4:0] raw5bit_LVDS6;  // LVDS0 data now
	reg [4:0] raw5bit_q_LVDS6;// LVDS0 data previous
   reg [4:0] raw5bit_qq_LVDS6;// LVDS0 data previoius previous
	reg [9:0] Finalword_LVDS6;// Final LVDS0 data word (Not in sync with "pclk")


	// Each LVDS data line will need these registers
   reg [4:0] raw5bit_LVDS7;  // LVDS0 data now
	reg [4:0] raw5bit_q_LVDS7;// LVDS0 data previous
   reg [4:0] raw5bit_qq_LVDS7;// LVDS0 data previoius previous
	reg [9:0] Finalword_LVDS7;// Final LVDS0 data word (Not in sync with "pclk")
	
//	 
	
	always @ (posedge pclkx2) begin


	raw5bit_Start   <=#1 sdoutStart;
   
	// Each LVDS data line will need these lines
	raw5bit_LVDS0   <=#1 sdout0;
   raw5bit_q_LVDS0 <=#1 raw5bit_LVDS0;
	raw5bit_qq_LVDS0 <=#1 raw5bit_q_LVDS0;

	// Each LVDS data line will need these lines
	raw5bit_LVDS1   <=#1 sdout1;
   raw5bit_q_LVDS1 <=#1 raw5bit_LVDS1;
	raw5bit_qq_LVDS1 <=#1 raw5bit_q_LVDS1;

	// Each LVDS data line will need these lines
	raw5bit_LVDS2   <=#1 sdout2;
   raw5bit_q_LVDS2 <=#1 raw5bit_LVDS2;
	raw5bit_qq_LVDS2 <=#1 raw5bit_q_LVDS2;

	// Each LVDS data line will need these lines
	raw5bit_LVDS3   <=#1 sdout3;
   raw5bit_q_LVDS3 <=#1 raw5bit_LVDS3;
	raw5bit_qq_LVDS3 <=#1 raw5bit_q_LVDS3;

	// Each LVDS data line will need these lines
	raw5bit_LVDS4   <=#1 sdout4;
   raw5bit_q_LVDS4 <=#1 raw5bit_LVDS4;
	raw5bit_qq_LVDS4 <=#1 raw5bit_q_LVDS4;

	 //Each LVDS data line will need these lines
	raw5bit_LVDS5   <=#1 sdout5;
   raw5bit_q_LVDS5 <=#1 raw5bit_LVDS5;
	raw5bit_qq_LVDS5 <=#1 raw5bit_q_LVDS5;

	// Each LVDS data line will need these lines
	raw5bit_LVDS6   <=#1 sdout6;
   raw5bit_q_LVDS6 <=#1 raw5bit_LVDS6;
	raw5bit_qq_LVDS6 <=#1 raw5bit_q_LVDS6;

	// Each LVDS data line will need these lines
	raw5bit_LVDS7   <=#1 sdout7;
   raw5bit_q_LVDS7 <=#1 raw5bit_LVDS7;
	raw5bit_qq_LVDS7 <=#1 raw5bit_q_LVDS7;
	
	
	
	
	
		if(raw5bit_Start != 5'b11111)  // We just create a word if there is a flag in start signal from ADC (remember that there is an inverter in the PCB, so the condition is when it is different to jus '1's)

			case (1'b0) // Word creation using Start signal from the ADC.   (We create the word depending in the place of the 5 bits where the start bit is active. In this case, when it is '0')

	 
			raw5bit_Start[0]: begin
											Finalword_LVDS0 <=#1 {                      raw5bit_q_LVDS0[4:0], raw5bit_LVDS0[4:0]};
											Finalword_LVDS1 <=#1 {                      raw5bit_q_LVDS1[4:0], raw5bit_LVDS1[4:0]};
											Finalword_LVDS2 <=#1 {                      raw5bit_q_LVDS2[4:0], raw5bit_LVDS2[4:0]};
											Finalword_LVDS3 <=#1 {                      raw5bit_q_LVDS3[4:0], raw5bit_LVDS3[4:0]};
											Finalword_LVDS4 <=#1 {                      raw5bit_q_LVDS4[4:0], raw5bit_LVDS4[4:0]};
											Finalword_LVDS5 <=#1 {                      raw5bit_q_LVDS5[4:0], raw5bit_LVDS5[4:0]};
											Finalword_LVDS6 <=#1 {                      raw5bit_q_LVDS6[4:0], raw5bit_LVDS6[4:0]};
											Finalword_LVDS7 <=#1 {                      raw5bit_q_LVDS7[4:0], raw5bit_LVDS7[4:0]};
									end
			raw5bit_Start[1]: begin
											Finalword_LVDS0 <=#1 {raw5bit_qq_LVDS0[0]  ,raw5bit_q_LVDS0[4:0], raw5bit_LVDS0[4:1]};
											Finalword_LVDS1 <=#1 {raw5bit_qq_LVDS1[0]  ,raw5bit_q_LVDS1[4:0], raw5bit_LVDS1[4:1]};
											Finalword_LVDS2 <=#1 {raw5bit_qq_LVDS2[0]  ,raw5bit_q_LVDS2[4:0], raw5bit_LVDS2[4:1]};
											Finalword_LVDS3 <=#1 {raw5bit_qq_LVDS3[0]  ,raw5bit_q_LVDS3[4:0], raw5bit_LVDS3[4:1]};
											Finalword_LVDS4 <=#1 {raw5bit_qq_LVDS4[0]  ,raw5bit_q_LVDS4[4:0], raw5bit_LVDS4[4:1]};
											Finalword_LVDS5 <=#1 {raw5bit_qq_LVDS5[0]  ,raw5bit_q_LVDS5[4:0], raw5bit_LVDS5[4:1]};
											Finalword_LVDS6 <=#1 {raw5bit_qq_LVDS6[0]  ,raw5bit_q_LVDS6[4:0], raw5bit_LVDS6[4:1]};
											Finalword_LVDS7 <=#1 {raw5bit_qq_LVDS7[0]  ,raw5bit_q_LVDS7[4:0], raw5bit_LVDS7[4:1]};
									end

 

			raw5bit_Start[2]: begin
											Finalword_LVDS0 <=#1 {raw5bit_qq_LVDS0[1:0],raw5bit_q_LVDS0[4:0], raw5bit_LVDS0[4:2]};
											Finalword_LVDS1 <=#1 {raw5bit_qq_LVDS1[1:0],raw5bit_q_LVDS1[4:0], raw5bit_LVDS1[4:2]};
											Finalword_LVDS2 <=#1 {raw5bit_qq_LVDS2[1:0],raw5bit_q_LVDS2[4:0], raw5bit_LVDS2[4:2]};
											Finalword_LVDS3 <=#1 {raw5bit_qq_LVDS3[1:0],raw5bit_q_LVDS3[4:0], raw5bit_LVDS3[4:2]};											
											Finalword_LVDS4 <=#1 {raw5bit_qq_LVDS4[1:0],raw5bit_q_LVDS4[4:0], raw5bit_LVDS4[4:2]};
											Finalword_LVDS5 <=#1 {raw5bit_qq_LVDS5[1:0],raw5bit_q_LVDS5[4:0], raw5bit_LVDS5[4:2]};
											Finalword_LVDS6 <=#1 {raw5bit_qq_LVDS6[1:0],raw5bit_q_LVDS6[4:0], raw5bit_LVDS6[4:2]};
											Finalword_LVDS7 <=#1 {raw5bit_qq_LVDS7[1:0],raw5bit_q_LVDS7[4:0], raw5bit_LVDS7[4:2]};											
									end
									
			raw5bit_Start[3]: begin
											Finalword_LVDS0 <=#1 {raw5bit_qq_LVDS0[2:0],raw5bit_q_LVDS0[4:0], raw5bit_LVDS0[4:3]};
											Finalword_LVDS1 <=#1 {raw5bit_qq_LVDS1[2:0],raw5bit_q_LVDS1[4:0], raw5bit_LVDS1[4:3]};
									      Finalword_LVDS2 <=#1 {raw5bit_qq_LVDS2[2:0],raw5bit_q_LVDS2[4:0], raw5bit_LVDS2[4:3]};
											Finalword_LVDS3 <=#1 {raw5bit_qq_LVDS3[2:0],raw5bit_q_LVDS3[4:0], raw5bit_LVDS3[4:3]};
											Finalword_LVDS4 <=#1 {raw5bit_qq_LVDS4[2:0],raw5bit_q_LVDS4[4:0], raw5bit_LVDS4[4:3]};
											Finalword_LVDS5 <=#1 {raw5bit_qq_LVDS5[2:0],raw5bit_q_LVDS5[4:0], raw5bit_LVDS5[4:3]};
											Finalword_LVDS6 <=#1 {raw5bit_qq_LVDS6[2:0],raw5bit_q_LVDS6[4:0], raw5bit_LVDS6[4:3]};
											Finalword_LVDS7 <=#1 {raw5bit_qq_LVDS7[2:0],raw5bit_q_LVDS7[4:0], raw5bit_LVDS7[4:3]};
											
									end
									
									
			raw5bit_Start[4]: begin
											Finalword_LVDS0 <=#1 {raw5bit_qq_LVDS0[3:0],raw5bit_q_LVDS0[4:0], raw5bit_LVDS0[4:4]};
											Finalword_LVDS1 <=#1 {raw5bit_qq_LVDS1[3:0],raw5bit_q_LVDS1[4:0], raw5bit_LVDS1[4:4]};
											Finalword_LVDS2 <=#1 {raw5bit_qq_LVDS2[3:0],raw5bit_q_LVDS2[4:0], raw5bit_LVDS2[4:4]};
											Finalword_LVDS3 <=#1 {raw5bit_qq_LVDS3[3:0],raw5bit_q_LVDS3[4:0], raw5bit_LVDS3[4:4]};
											Finalword_LVDS4 <=#1 {raw5bit_qq_LVDS4[3:0],raw5bit_q_LVDS4[4:0], raw5bit_LVDS4[4:4]};
											Finalword_LVDS5 <=#1 {raw5bit_qq_LVDS5[3:0],raw5bit_q_LVDS5[4:0], raw5bit_LVDS5[4:4]};
											Finalword_LVDS6 <=#1 {raw5bit_qq_LVDS6[3:0],raw5bit_q_LVDS6[4:0], raw5bit_LVDS6[4:4]};
											Finalword_LVDS7 <=#1 {raw5bit_qq_LVDS7[3:0],raw5bit_q_LVDS7[4:0], raw5bit_LVDS7[4:4]};

									end
	 

			endcase
		else   // If there is no flag in start signal from ADC, we don't do anything (so we change from "pclkx2" to "pclk" )
		;
 
	end



// here we will sync the 10 bits word to the slow clock pclk. 

	reg [9:0] FinalSyncword_LVDS0;
	reg [9:0] FinalSyncword_LVDS1;
	reg [9:0] FinalSyncword_LVDS2;
	reg [9:0] FinalSyncword_LVDS3;
	reg [9:0] FinalSyncword_LVDS4;
	reg [9:0] FinalSyncword_LVDS5;
	reg [9:0] FinalSyncword_LVDS6;
	reg [9:0] FinalSyncword_LVDS7; 
	
	reg [79:0] FinalSyncword_LVDS;

	 


	always @ (posedge pclk) begin
	
	FinalSyncword_LVDS0=Finalword_LVDS0;      //To sync the word with the x1 clock "pclk"
	FinalSyncword_LVDS1=Finalword_LVDS1;      //To sync the word with the x1 clock "pclk"
	FinalSyncword_LVDS2=Finalword_LVDS2;      //To sync the word with the x1 clock "pclk"
	FinalSyncword_LVDS3=Finalword_LVDS3;      //To sync the word with the x1 clock "pclk"
	FinalSyncword_LVDS4=Finalword_LVDS4;      //To sync the word with the x1 clock "pclk"
	FinalSyncword_LVDS5=Finalword_LVDS5;      //To sync the word with the x1 clock "pclk"
	FinalSyncword_LVDS6=Finalword_LVDS6;      //To sync the word with the x1 clock "pclk"
	FinalSyncword_LVDS7=Finalword_LVDS7;      //To sync the word with the x1 clock "pclk"
 		 

// This was the way to concatenate all the synced words. I did not used at the and as in the PCB there is an inverter. The solution so far is to invert each bit. There must be a way to do it in a cleaner way. 

//   FinalSyncword_LVDS <=#1 {FinalSyncword_LVDS0,FinalSyncword_LVDS1,FinalSyncword_LVDS2,FinalSyncword_LVDS3,FinalSyncword_LVDS4,FinalSyncword_LVDS5,FinalSyncword_LVDS6,FinalSyncword_LVDS7};
//   sdout=FinalSyncword_LVDS;



// This part here could be improved 
	
   sdout[70]=~Finalword_LVDS0[0];
	sdout[71]=~Finalword_LVDS0[1];
	sdout[72]=~Finalword_LVDS0[2];
	sdout[73]=~Finalword_LVDS0[3];
	sdout[74]=~Finalword_LVDS0[4];
	sdout[75]=~Finalword_LVDS0[5];
	sdout[76]=~Finalword_LVDS0[6];
	sdout[77]=~Finalword_LVDS0[7];
	sdout[78]=~Finalword_LVDS0[8];
	sdout[79]=~Finalword_LVDS0[9];

   sdout[60]=~Finalword_LVDS1[0];
	sdout[61]=~Finalword_LVDS1[1];
	sdout[62]=~Finalword_LVDS1[2];
	sdout[63]=~Finalword_LVDS1[3];
	sdout[64]=~Finalword_LVDS1[4];
	sdout[65]=~Finalword_LVDS1[5];
	sdout[66]=~Finalword_LVDS1[6];
	sdout[67]=~Finalword_LVDS1[7];
	sdout[68]=~Finalword_LVDS1[8];
	sdout[69]=~Finalword_LVDS1[9];

   sdout[50]=~Finalword_LVDS2[0];
	sdout[51]=~Finalword_LVDS2[1];
	sdout[52]=~Finalword_LVDS2[2];
	sdout[53]=~Finalword_LVDS2[3];
	sdout[54]=~Finalword_LVDS2[4];
	sdout[55]=~Finalword_LVDS2[5];
	sdout[56]=~Finalword_LVDS2[6];
	sdout[57]=~Finalword_LVDS2[7];
	sdout[58]=~Finalword_LVDS2[8];
	sdout[59]=~Finalword_LVDS2[9];

   sdout[40]=~Finalword_LVDS3[0];
	sdout[41]=~Finalword_LVDS3[1];
	sdout[42]=~Finalword_LVDS3[2];
	sdout[43]=~Finalword_LVDS3[3];
	sdout[44]=~Finalword_LVDS3[4];
	sdout[45]=~Finalword_LVDS3[5];
	sdout[46]=~Finalword_LVDS3[6];
	sdout[47]=~Finalword_LVDS3[7];
	sdout[48]=~Finalword_LVDS3[8];
	sdout[49]=~Finalword_LVDS3[9];
	
	sdout[30]=~Finalword_LVDS4[0];
	sdout[31]=~Finalword_LVDS4[1];
	sdout[32]=~Finalword_LVDS4[2];
	sdout[33]=~Finalword_LVDS4[3];
	sdout[34]=~Finalword_LVDS4[4];
	sdout[35]=~Finalword_LVDS4[5];
	sdout[36]=~Finalword_LVDS4[6];
	sdout[37]=~Finalword_LVDS4[7];
	sdout[38]=~Finalword_LVDS4[8];
	sdout[39]=~Finalword_LVDS4[9];
	
	sdout[20]=~Finalword_LVDS5[0];
	sdout[21]=~Finalword_LVDS5[1];
	sdout[22]=~Finalword_LVDS5[2];
	sdout[23]=~Finalword_LVDS5[3];
	sdout[24]=~Finalword_LVDS5[4];
	sdout[25]=~Finalword_LVDS5[5];
	sdout[26]=~Finalword_LVDS5[6];
	sdout[27]=~Finalword_LVDS5[7];
	sdout[28]=~Finalword_LVDS5[8];
	sdout[29]=~Finalword_LVDS5[9];
	
	sdout[10]=~Finalword_LVDS6[0];
	sdout[11]=~Finalword_LVDS6[1];
	sdout[12]=~Finalword_LVDS6[2];
	sdout[13]=~Finalword_LVDS6[3];
	sdout[14]=~Finalword_LVDS6[4];
	sdout[15]=~Finalword_LVDS6[5];
	sdout[16]=~Finalword_LVDS6[6];
	sdout[17]=~Finalword_LVDS6[7];
	sdout[18]=~Finalword_LVDS6[8];
	sdout[19]=~Finalword_LVDS6[9];

   sdout[0]=~Finalword_LVDS7[0];
	sdout[1]=~Finalword_LVDS7[1];
	sdout[2]=~Finalword_LVDS7[2];
	sdout[3]=~Finalword_LVDS7[3];
	sdout[4]=~Finalword_LVDS7[4];
	sdout[5]=~Finalword_LVDS7[5];
	sdout[6]=~Finalword_LVDS7[6];
	sdout[7]=~Finalword_LVDS7[7];
	sdout[8]=~Finalword_LVDS7[8];
	sdout[9]=~Finalword_LVDS7[9];

// This part here could be improved 



  ////////////////////////////////////////////////////////////////////////////////////////////////
  //
  // End:    Word Creation  
  //
  ////////////////////////////////////////////////////////////////////////////////////////////////

 	
	end
  
 
endmodule

