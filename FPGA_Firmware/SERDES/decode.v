//
//  File name :       decoder.v
//
//  Description :     Spartan-6 dvi decoder 
 
//
//////////////////////////////////////////////////////////////////////////////
  
   
`timescale 1 ns / 1ps

module decode (
 //These are input ports
  input  wire reset,            //
  input  wire pclk,             //  pixel clock
  input  wire pclkx2,           //  double pixel rate for gear box
  input  wire pclkx10,          //  IOCLK
  input  wire serdesstrobe,     //  serdesstrobe for iserdes2
  input  wire din_p,            //  data from dvi cable
  input  wire din_n,            //  data from dvi cable
   
 
  //These are output ports

  output reg [4:0] sdout);      // dATA OUTPUT. 
  
  
  ////////////////////////////////
  //
  // Changes order of bits. MSB first.
  //
  ////////////////////////////////
  

   wire [4:0] raw5bit;
   reg [4:0] raw5bit_MSB_First;
 
 // Exchange the bit order to put the MSB first
  always @ (posedge pclkx2) begin
	
	raw5bit_MSB_First[0]    <=#1 raw5bit[4];   // Exchange the bit order to put the MSB first
	raw5bit_MSB_First[1]    <=#1 raw5bit[3];
	raw5bit_MSB_First[2]    <=#1 raw5bit[2];
	raw5bit_MSB_First[3]    <=#1 raw5bit[1];
	raw5bit_MSB_First[4]    <=#1 raw5bit[0];

	sdout = raw5bit_MSB_First;

  end

  
  
  ////////////////////////////////
  //
  // bitslip signal sync to pclkx2   
  //
  ////////////////////////////////
  reg bitslipx2 = 1'b0;    
  reg bitslip_q = 1'b0;
  wire bitslip;

  always @ (posedge pclkx2) begin
    bitslip_q <=#1 bitslip;
    bitslipx2 <=#1 bitslip & !bitslip_q;
  end 

  
  
  /////////////////////////////////////////////
  //
  // 1:5 de-serializer working at x2 pclk rate
  //
  /////////////////////////////////////////////
 
 serdes_1_to_5_diff_data # (
    .DIFF_TERM("FALSE"),
    .BITSLIP_ENABLE("TRUE")
  ) des_0 (
   //These are input ports
    .use_phase_detector(1'b1),
    .datain_p(din_p),
    .datain_n(din_n),
    .rxioclk(pclkx10),
    .rxserdesstrobe(serdesstrobe),
    .reset(reset),
    .gclk(pclkx2),
	 .bitslip(bitslipx2),
	   
	//These are output ports
 	 .data_out(raw5bit)
  );

  
  
endmodule

