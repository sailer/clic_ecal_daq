 --Reference Clock. 
 
 -- Description:
 -- It gets the 100 MHZ system clock and Generates two clocks: 1) 100MHz to use as system clock: "CLK_OUT_slow"
 --                                                            2) 200MHz differential clock to use as reference for the ADC. (the frequency can be changed): "CLK_OUT_p" "CLK_OUT_n"
 
 
 -----------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;




entity Ref_Clock_ADC is
 
port
 (-- Clock in ports
   CLK_IN1           : in  std_logic;
 	
	CLK_OUT_slow    : out std_logic ;
	CLK_OUT_p         : out std_logic;
	CLK_OUT_n         : out std_logic;
   -- Status and control signals
  RESET             : in  std_logic;
  LOCKED            : out std_logic
 );
end Ref_Clock_ADC;





architecture xilinx of Ref_Clock_ADC is

  -- Parameters for the counters
  ---------------------------------
  -- Counter width
 

  signal   locked_int : std_logic;
  signal   reset_int  : std_logic                     := '0';

  -- Declare the clocks and counter
  signal   clk_int, clk_int2     : std_logic;
  signal   clk, clk_n            : std_logic;
 

component Clock_ADC_Generator is
port
 (-- Clock in ports
  CLK_IN1           : in     std_logic;
  -- Clock out ports
  CLK_OUT1          : out    std_logic;  
  CLK_OUT2          : out    std_logic;
  -- Status and control signals
  RESET             : in     std_logic;
  LOCKED            : out    std_logic
 );
end component;



begin
  -- Alias output to internally used signal
  LOCKED    <= locked_int;

  -- Instantiation of the clocking network
  -----------------------------------------
  

  --              |¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥¥\  --    clk_int -> (clk & clk_n) --|¥¥¥¥¥¥¥¥\                 |¥¥¥¥¥¥|-- CLK_OUT_p
  --    CLK_IN1 --| Clock_ADC_Generator  >                                  | ODDR2   >-- clk_int2  -- |OBUFDS| 
  --              |_____________________/  --    CLK_OUT_slow               |________/                 |______|-- CLK_OUT_n


  --               Clock_ADC_Generator_inst                                 clkout_oddr                OBUFDS_inst       


Clock_ADC_Generator_inst : Clock_ADC_Generator
  port map
   (-- Clock in ports
    CLK_IN1 => CLK_IN1,  --100 MHz
  
    -- Clock out ports
    CLK_OUT1 => clk_int, --Fast Clock (has to be processed into differential pair) 
    CLK_OUT2 => CLK_OUT_slow,--slow clock (it is ready here)
  
    -- Status and control signals
    RESET  => RESET,
    LOCKED => locked_int);
   
	
	
	--create a differential LVDS clock
	
	clk <= clk_int;        --generated to use in ODDR2 and create a differential LVDS clock with OBUFDS
   clk_n <= not clk;      --generated to use in ODDR2 and create a differential LVDS clock with OBUFDS


 clkout_oddr : ODDR2
  port map
   (
    Q  => clk_int2,  -- The output 
	 C0 => clk,       -- input p
    C1 => clk_n,     -- input n
    CE => '1',
    D0 => '1',
    D1 => '0',
    R  => '0',
    S  => '0');

    
 --To get a differential LVDS clock at the output
   OBUFDS_inst : OBUFDS
 	port map (
   O => CLK_OUT_p,    -- Diff_p output (connect directly to top-level port)
   OB => CLK_OUT_n,   -- Diff_n output (connect directly to top-level port)
   I => clk_int2      -- Buffer input 
);

end xilinx;

