------------------------------------------------------------------------------
-- "Output    Output      Phase     Duty      Pk-to-Pk        Phase"
-- "Clock    Freq (MHz) (degrees) Cycle (%) Jitter (ps)  Error (ps)"
------------------------------------------------------------------------------
-- CLK_OUT1___200.000______0.000_______N/A______220.000________N/A
-- CLK_OUT2___100.000______0.000_______N/A________0.000________N/A
--
------------------------------------------------------------------------------
-- "Input Clock   Freq (MHz)    Input Jitter (UI)"
------------------------------------------------------------------------------
-- __primary_________100.000____________0.010


--Selection of the frequency of slow and fast clock. 
--
-- 1) Frequency clock fast: = CLKFX frequency => F_CLKFX = F_CLKIN * CLKFX_MULTIPLY/ CLKFX_DIVIDE. 
-- Example : Let's say that the freq of the input clock is 100MHZ (F_CLKIN=100MHz). If we want a 200MHz clock we have to choose:  CLKFX_MULTIPLY=2 and CLKFX_DIVIDE=1. 

-- 2)  Frequency clock Slow: = CLKFXDV freq   => F_CLKFXDV =F_CLKFX /CLKFXDV_DIVIDE.  
-- Example: If we want 100MHz of slow clock, then : CLKFXDV_DIVIDE=2; 




library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;


entity Clock_ADC_Generator is
port
 (-- Clock in ports
  CLK_IN1           : in     std_logic;

  -- Clock out ports
  CLK_OUT1          : out    std_logic;
  CLK_OUT2          : out    std_logic;

  -- Status and control signals
  RESET             : in     std_logic;
  LOCKED            : out    std_logic
 );
end Clock_ADC_Generator;



architecture xilinx of Clock_ADC_Generator is
  --attribute CORE_GENERATION_INFO : string;
  --attribute CORE_GENERATION_INFO of xilinx : architecture is "Clock_ADC_200MHz,clk_wiz_v3_6,{component_name=Clock_ADC_200MHz,use_phase_alignment=false,use_min_o_jitter=false,use_max_i_jitter=false,use_dyn_phase_shift=false,use_inclk_switchover=false,use_dyn_reconfig=false,feedback_source=FDBK_AUTO,primtype_sel=DCM_CLKGEN,num_out_clk=2,clkin1_period=10.0,clkin2_period=10.0,use_power_down=false,use_reset=true,use_locked=true,use_inclk_stopped=false,use_status=false,use_freeze=false,use_clk_valid=false,feedback_type=SINGLE,clock_mgr_type=MANUAL,manual_override=false}";
  -- Input clock buffering / unused connectors
  signal clkin1            : std_logic;
  -- Output clock buffering / unused connectors
  signal clkfx             : std_logic;
  signal clkfx180_unused   : std_logic;
  signal clkfxdv           : std_logic;
  signal clkfbout          : std_logic;
  -- Dynamic programming unused signals
  signal progdone_unused   : std_logic;
  signal locked_internal   : std_logic;
  signal status_internal   : std_logic_vector(2 downto 1);

begin


  -- Input buffering
  --------------------------------------
  clkin1_buf : IBUFG
  port map
   (O => clkin1,
    I => CLK_IN1);


  -- Clocking primitive
  --------------------------------------
  -- Instantiation of the DCM primitive
  --    * Unused inputs are tied off
  --    * Unused outputs are labeled unused
  dcm_clkgen_inst: DCM_CLKGEN
  generic map
   (CLKFXDV_DIVIDE        => 2,     --Defines the division factor for the frequency of the CLKFXDV output. Allowable values are 2, 4, 8, 16, 32. Default value is 2.
    CLKFX_DIVIDE          => 1,     --change to 1 to make 200MHz    --Defines the division factor for the frequency of the CLKFX. Allowable values: integers ranging from 1 to 256. Default value is 1.
    CLKFX_MULTIPLY        => 2,     --Defines the multiplication factor for the frequency of the CLKFX. Allowable values:integers ranging from 2 to 256. Default value is 4.
    SPREAD_SPECTRUM       => "NONE",
    STARTUP_WAIT          => FALSE,
    CLKIN_PERIOD          => 10.0,
    CLKFX_MD_MAX          => 0.000)
  port map
   -- Input clock
   (CLKIN                 => clkin1,    --Clock Input
    -- Output clocks
    CLKFX                 => clkfx,     --Clock Output -- F_CLKFX = F_CLKIN * CLKFX_MULTIPLY/ CLKFX_DIVIDE
    CLKFX180              => clkfx180_unused, --appears to be an inverted version of CLKFX
    CLKFXDV               => CLK_OUT2,  --Divided CLKFX output clock.--F_CLKFXDV  =F_CLKFX /CLKFXDV_DIVIDE 
													 
	
	-- Ports for dynamic phase shift
    PROGCLK               => '0',
    PROGEN                => '0',
    PROGDATA              => '0',
    PROGDONE              => progdone_unused,
   -- Other control and status signals
    FREEZEDCM             => '0',
    LOCKED                => locked_internal,
    STATUS                => status_internal,
    RST                   => RESET);

  LOCKED                <= locked_internal;

  -- Output buffering
  -------------------------------------
  
-- Fast clock buffer  
  clkout1_buf : BUFG
  port map
   (O   => CLK_OUT1,
    I   => clkfx);
	  
	 
-- Slow clock output 
--Not needed at the end (uncomment if global buffer is needed)
--  clkout2_buf : BUFG   
--  port map
--   (O   => CLK_OUT2,
--    I   => clkfxdv);

end xilinx;

