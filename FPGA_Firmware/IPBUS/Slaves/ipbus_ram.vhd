-- Generic ipbus ram block for testing
--
-- generic addr_width defines number of significant address bits
--
-- In order to allow Xilinx block RAM to be inferred:
-- 	Reset does not clear the RAM contents (not implementable in Xilinx)
--		There is one cycle of latency on the read / write
--
-- Note that the synthesis tool should automatically infer block or distributed RAM
-- according to the size requested. It is likely that it will NOT choose
-- an efficient implementation in terms of area / speed / power, so don't use this
-- method to infer large RAMs (noting also that reads are enabled at all times).
-- It's best to use the block ram core generator explicitly.
--
-- Occupies addr_width bits of ipbus address space
-- This RAM cannot be used with 100% bus utilisation due to the wait state
--
-- Dave Newbold, March 2011
--
-- $Id: ipbus_ram.vhd 1201 2012-09-28 08:49:12Z phdmn $

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.ipbus.all;

entity ipbus_ram is
	generic(addr_width : positive);
	port(
		datain: in STD_LOGIC_VECTOR(79 downto 0);
      clockin: IN std_logic;
		Flag_readout: IN std_logic;
				
		clk: in STD_LOGIC;
		reset: in STD_LOGIC;
		ipbus_in: in ipb_wbus;
		ipbus_out: out ipb_rbus
	);
	
end ipbus_ram;

architecture rtl of ipbus_ram is

	type reg_array is array(2**addr_width-1 downto 0) of std_logic_vector(31 downto 0);
	signal reg: reg_array;

	signal sel_w: integer:=0;
 
	signal ack: std_logic;
 
   signal wr_en_FIFO : STD_LOGIC_VECTOR(0 DOWNTO 0) := "0";
   signal rd_en_FIFO : STD_LOGIC_VECTOR(0 DOWNTO 0) := "0";
	
   signal   addra:  STD_LOGIC_VECTOR(9 DOWNTO 0):= "0000000000";
   signal  douta : STD_LOGIC_VECTOR(31 DOWNTO 0);
   signal  dinb : STD_LOGIC_VECTOR(31 DOWNTO 0);

	signal sel: STD_LOGIC_VECTOR(9 DOWNTO 0);
 
   signal datain_part: STD_LOGIC_VECTOR(19 DOWNTO 0);
 
 COMPONENT RAM
  PORT (
    clka : IN STD_LOGIC;
    wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addra : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    dina : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    douta : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    clkb : IN STD_LOGIC;
    web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addrb : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    dinb : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    doutb : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
  );
END COMPONENT;
 
begin


 
  
  inst_RAM : RAM
  PORT MAP (
    clka => clockin,
    wea => wr_en_FIFO,
    addra => addra,
    dina => addra & "00"& datain_part,
    douta => douta,
    clkb => clk,
    web => rd_en_FIFO,
    addrb => sel,
    dinb => dinb,
    doutb => ipbus_out.ipb_rdata
  );
  



--Writing part 
	
	process(clockin)
	begin
		if rising_edge(clockin) then
 				
 				
	
				if       Flag_readout='1'  then 
				
   			        if  sel_w = 100000000  then   			 
					        wr_en_FIFO<="1";
                       addra<= std_logic_vector(unsigned(addra)+0);
							  datain_part<= datain(19 downto 0);
							  sel_w<=sel_w+1;
				
				
   			        elsif  sel_w > 100000000 and sel_w<100000256 then   					 
					        wr_en_FIFO<="1";
                       addra<= std_logic_vector(unsigned(addra)+1);
							  datain_part<= datain(19 downto 0);
                       sel_w<=sel_w+1;
						
				        elsif  sel_w > 100000255 and sel_w<100000512 then   				 
							  wr_en_FIFO<="1";
                       addra<= std_logic_vector(unsigned(addra)+1);
							  datain_part<= datain(39 downto 20);
                       sel_w<=sel_w+1;
						
						  elsif  sel_w > 100000511 and sel_w<100000768 then   				 
							  wr_en_FIFO<="1";
                       addra<= std_logic_vector(unsigned(addra)+1);
							  datain_part<= datain(59 downto 40);
                       sel_w<=sel_w+1;
				
				        elsif  sel_w > 100000767 and sel_w<100001024 then   				 
							  wr_en_FIFO<="1";
                       addra<= std_logic_vector(unsigned(addra)+1);
							  datain_part<= datain(79 downto 60);
                       sel_w<=sel_w+1;
						
   						elsif sel_w = 200000000 then
							
							sel_w <=0;
							 
							wr_en_FIFO<="0";

				         else 
						      sel_w<=sel_w+1;
					        wr_en_FIFO<="0";

				        end if; 
		      end if; 

			
		end if;
	end process;





--Reading part 
	sel <=  ipbus_in.ipb_addr(addr_width-1 downto 0);

--	sel <= to_integer(to_unsigned(ipbus_in.ipb_addr(addr_width-1 downto 0)));

	process(clk)
	begin
		if rising_edge(clk) then
--		--	if ipbus_in.ipb_strobe='1' and ipbus_in.ipb_write='1' then
--		--		reg(sel) <= ipbus_in.ipb_wdata;
--		--	end if;
-- 
--			
--			
--			ipbus_out.ipb_rdata <= reg(sel);
			ack <= ipbus_in.ipb_strobe and not ack;
--			
		end if;
	end process;
--	
   --ack <= ipbus_in.ipb_strobe and not ack;
	ipbus_out.ipb_ack <= ack;
	ipbus_out.ipb_err <= '0';
end rtl;
